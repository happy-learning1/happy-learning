-- Create the database
CREATE DATABASE Course_Online;
GO

-- Use the newly created database
USE Course_Online;
GO

-- Create the role table
CREATE TABLE role (
    role_id INT PRIMARY KEY,
    role_name VARCHAR(50) NOT NULL
);
GO

-- Create the skill table
CREATE TABLE skill (
    skill_id INT PRIMARY KEY,
    skill_name VARCHAR(50) NOT NULL
);
GO

-- Create the user table
CREATE TABLE [user] (
    user_id INT PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    [password] VARCHAR(255) NOT NULL, -- Adjust the length based on your security requirements
    fullname VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone_number VARCHAR(20),
    --skill_id INT,
    role_id INT,
    [status] VARCHAR(20),
	avatar VARCHAR(100),
    --rate DECIMAL(10, 2),
    balance DECIMAL(10, 2),
    --FOREIGN KEY (skill_id) REFERENCES skill(skill_id),
    FOREIGN KEY (role_id) REFERENCES role(role_id)
);
GO

--Create the user_skill table
CREATE TABLE user_skill(
	user_id INT,
	skill_id INT,
	PRIMARY KEY (user_id, skill_id),
	FOREIGN KEY (skill_id) REFERENCES skill(skill_id),
	FOREIGN KEY (user_id) REFERENCES [user](user_id),
);
GO

-- Create the subject table
CREATE TABLE subject (
	mentor_id INT,
    subject_id INT PRIMARY KEY,
    subject_name VARCHAR(50) NOT NULL,
    subject_detail VARCHAR(MAX),
	subject_image VARCHAR(100),
	FOREIGN KEY (mentor_id) REFERENCES [user](user_id),
);
GO

-- Create the request table
CREATE TABLE request (
    mentee_id INT,
    mentor_id INT,
    title VARCHAR(100) NOT NULL,
    content VARCHAR(MAX) NOT NULL,
    deadline DATE,
    schedule DATETIME,
    status VARCHAR(20),
    PRIMARY KEY (mentee_id, mentor_id),
    FOREIGN KEY (mentee_id) REFERENCES [user](user_id),
    FOREIGN KEY (mentor_id) REFERENCES [user](user_id)
);
GO

-- Create the rate table
CREATE TABLE rate (
    mentee_id INT,
    mentor_id INT,
    comment VARCHAR(MAX),
    rate INT,
    PRIMARY KEY (mentee_id, mentor_id),
    FOREIGN KEY (mentee_id) REFERENCES [user](user_id),
    FOREIGN KEY (mentor_id) REFERENCES [user](user_id)
);
GO

-- Create the cv table
CREATE TABLE cv (
    id INT PRIMARY KEY,
    mentor_id INT,
    age INT,
    skill VARCHAR(50),
	introduction VARCHAR(500),
	schedule VARCHAR(200),
    experience VARCHAR(MAX),
    FOREIGN KEY (mentor_id) REFERENCES [user](user_id)
);
GO

-- Create the slot table
CREATE TABLE slot (
	slot_id INT PRIMARY KEY,
	start_time DATETIME,
	end_time DATETIME,
)

-- Create the schedule table
CREATE TABLE schedule (
    mentor_id INT,
    slot_id INT,
    subject_id INT,
	[status] varchar(100),
    PRIMARY KEY (mentor_id, slot_id),
	FOREIGN KEY (slot_id) REFERENCES slot(slot_id),
    FOREIGN KEY (mentor_id) REFERENCES [user](user_id),
    FOREIGN KEY (subject_id) REFERENCES subject(subject_id)
);
GO

-- Create the payment table
CREATE TABLE payment (
    payment_id INT PRIMARY KEY,
    user_id INT,
    time DATETIME,
    cost DECIMAL(10, 2),
    balance DECIMAL(10, 2),
    FOREIGN KEY (user_id) REFERENCES [user](user_id)
);
GO
