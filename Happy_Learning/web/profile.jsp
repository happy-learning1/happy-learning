<%-- 
    Document   : profile
    Created on : Jan 11, 2024, 12:33:16 PM
    Author     : Giang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.User" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3><a href="#" style="text-decoration: none">Back</a></h3>
        <h3>Your infomation</h3>
        Username: ${user.username} <br>
        Fullname: ${user.fullname} <br>
        Email: ${user.email} <br>
        Phone number: ${user.phone_number} <br>
        <hr>
        <h3>---- Update profile ----</h3>                
        <form action="profile" method="post">
            Username:<input type="text" value="${user.username}" name="username"/><br>
            Password:<input type="text" value="${user.password}" name="password"/><br>
            Fullname:<input type="text" value="${user.fullname}" name="fullname"/><br>
            Email:<input type="text" value="${user.email}" readonly/><br>
            Phone number:<input type="text" value="${user.phone_number}" name="phone_number"/><br>
            <input type="submit" value="Save" name="submit_value"/>  
            <button onclick="window.location.href='profile'">Cancel</button>
        </form>
        
    </body>
</html>
