/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.User;
import java.util.regex.*;

/**
 *
 * @author Giang
 */
@WebServlet(name="profileServlet", urlPatterns={"/profile"})
public class profileServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet profileServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet profileServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //add user id when having login function to help indicate what user is
        //for testing function, user_id = 1 is being used
        UserDao udao = new UserDao();
        User u = udao.getProfile(1);
        request.setAttribute("user", u);
        request.getRequestDispatcher("profile.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        UserDao udao = new UserDao();
        //add user id when having login function to help indicate what user is
        //for testing function, user_id = 1 is being used
        User u = udao.getProfile(1);
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String fullname = request.getParameter("fullname");
        String phone_number = request.getParameter("phone_number");
        User u2 = u;
        if(ValidateUsername(username))u2.setUsername(username);
        if(ValidatePassword(password))u2.setPassword(password);
        if(ValidateFullname(fullname))u2.setFullname(fullname);
        if(ValidatePhoneNumber(phone_number))u2.setPhone_number(phone_number);
        udao.updateProfile(u2);
        request.setAttribute("user", u2);
        request.getRequestDispatcher("profile.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public static boolean ValidateUsername(String username){
        Pattern pattern = Pattern.compile("\\A(?=.*[A-Z])(?=.*\\d)[a-zA-Z0-9]{4,}\\z");
        Matcher match = pattern.matcher(username);
        return match.find();
    }
    
    public static boolean ValidatePassword(String password){
        //add this in the pattern for required of having at least one special character in password (?=.*?[#?!@$%^&*-])
        Pattern pattern = Pattern.compile("^(?!.*\\s)(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$");
        Matcher match = pattern.matcher(password);
        return match.find();
    }
    
    public static boolean ValidateFullname(String fullname){
        //change the value of number inside {} to change the strict length for full name, preview with 7 character only
        Pattern pattern = Pattern.compile("^(?!.*?[#?!@$%^&*-])(?!.*?[0-9])[a-zA-Z\\s]{8,}$");
        Matcher match = pattern.matcher(fullname);
        return match.find();
    }
    
    public static boolean ValidatePhoneNumber(String phone_number){
        Pattern pattern = Pattern.compile("^(?!.*?[#?!@$%^&*-])(?!.*?[a-zA-Z])[0-9]{10,}$");
        Matcher match = pattern.matcher(phone_number);
        return match.find();
    }
    
    public static void main(String[] args) {
        System.out.println(ValidateUsername("aHi123"));
    }
}
